### typescript环境搭建：
1.安装typescript编译器（npm install -g typescript），安装后可在命令行中使用'tsc'命令。
2.初始化Node项目(npm init -y),生成package.json文件，包括项目的相关信息。
package.json文件中，配置"scripts"中项"dev":"node app.js";命令行中npm run dev调试项目。
3.创建TypeScript配置文件（tsc --init）,生成tsconfig.json。
4.tsconfig.json中设置 "rootDir": "./src";"outDir": "./bin";"sourceMap": true;"module": "commonjs";  "target": "es2016"。
5.编写ts文件，文件夹根目录运行tsc编译，执行bin夹中js文件(node *.js)

### typescript开发express环境搭建
1.基础配置如上；
2.安装Express框架和TypeScript声明文件
(npm install express)/(npm install @types/express)