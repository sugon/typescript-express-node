"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http = (0, express_1.default)();
http.get('/', (req, res) => {
    res.end('express server');
});
http.listen(3000, () => {
    console.log('running port 3000...');
});
//# sourceMappingURL=app.js.map