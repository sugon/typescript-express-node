import express from 'express';
const http = express();
http.get('/', (req, res) => {
    res.end('express server...');
})
http.listen(3000, () => {
    console.log('running port 3000...')
})